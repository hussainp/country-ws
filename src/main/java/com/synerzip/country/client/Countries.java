package com.synerzip.country.client;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.user.client.ui.SubmitButton;
import com.smartgwt.client.core.BaseClass;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.DataSourceField;
import com.smartgwt.client.data.OperationBinding;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.WSDLLoadCallback;
import com.smartgwt.client.data.WebService;
import com.smartgwt.client.data.WebServiceCallback;
import com.smartgwt.client.data.XMLTools;
import com.smartgwt.client.data.XmlNamespaces;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.rpc.RPCResponse;
import com.smartgwt.client.types.DSOperationType;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.JSOHelper;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import com.synerzip.country.server.service.CountryService;

/*
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Countries implements EntryPoint {
	private static final String wsdlURL = "country.wsdl";
	private static final String namespaceURL = "http://synerzip.com/m/schemas";
	private static final String prefix = "message";
	
	/**
	 * CRUD operations over WS
	 */
	private static final String addOperation = "add";
	private static final String updateOperation = "update";
	private static final String removeOperation = "remove";
	private static final String getAllOperation = "getAll";
	
    public void onModuleLoad() {
    	final Canvas canvas = new Canvas();  
        canvas.setWidth100();  
        canvas.setHeight100();  

        SC.showPrompt("Loading WSDL from: " + wsdlURL);  

        XMLTools.loadWSDL(wsdlURL, new WSDLLoadCallback() {  
            public void execute(WebService service) {  
                if(service == null) {  
                    SC.warn("WSDL not currently available from LocalHost (tried "+ wsdlURL+ ")", new BooleanCallback() {  
                        public void execute(Boolean value) {  
                        }  
                    });  
                    return;  
                }
                
                final WebService countryWebService = service;
                
                SC.showPrompt("wsdl loaded");
                
                VLayout layout = new VLayout(20);  
                layout.setWidth100();  
                layout.setHeight100();  
                layout.setLayoutMargin(40);                  
                
                final ListGrid gridResults = new ListGrid();  
                gridResults.setWidth100();
                gridResults.setCanEdit(true);
                gridResults.setModalEditing(true);
                gridResults.setShowRowNumbers(true);
                
                ListGridField id = new ListGridField("Id");
                id.setType(ListGridFieldType.TEXT);
                id.setHidden(true);
                
                ListGridField name = new ListGridField("Name");
                name.setType(ListGridFieldType.TEXT);

                ListGridField code = new ListGridField("Code");
                code.setType(ListGridFieldType.TEXT);
                
                ListGridField continent = new ListGridField("Continent");
                continent.setType(ListGridFieldType.TEXT);
                
                ListGridField capital = new ListGridField("Capital");
                capital.setType(ListGridFieldType.TEXT);
                
                gridResults.setFields(name,code,capital,continent);
                
                final Button addButton = new Button("Add");
                layout.addMember(addButton);
                
                final Button removeButton = new Button("Remove");
                removeButton.disable();
                layout.addMember(removeButton);
                
                final Button updateButton = new Button("Update");
                updateButton.disable();
                
                updateButton.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						ListGridRecord selectedRecord = gridResults.getSelectedRecord();
						
						Map<String, String> paramData = new HashMap<String, String>();
						Map<String,Object> requestData = new HashMap<String, Object>();
						
						paramData.put("Name",selectedRecord.getAttribute("Name"));
						paramData.put("Capital",selectedRecord.getAttribute("Capital"));
						paramData.put("Code",selectedRecord.getAttribute("Code"));
						paramData.put("Id",selectedRecord.getAttribute("Id"));
						paramData.put("Continent",selectedRecord.getAttribute("Continent"));
						
						requestData.put("Country",JSOHelper.convertMapToJavascriptObject(paramData));
						
						countryWebService.callOperation(updateOperation, requestData, "//Country", new WebServiceCallback() {
							
							public void execute(Object[] data, JavaScriptObject xmlDoc,
									RPCResponse rpcResponse, JavaScriptObject wsRequest) {
							}
						});
						removeButton.disable();
						updateButton.disable();
					}
				});
                
                removeButton.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						ListGridRecord selectedRecord = gridResults.getSelectedRecord();
						
						Map<String,Object> requestData = new HashMap<String, Object>();
						
						Map<String, String> paramData = new HashMap<String, String>();
						
						paramData.put("Name",selectedRecord.getAttribute("Name"));
						paramData.put("Capital",selectedRecord.getAttribute("Capital"));
						paramData.put("Code",selectedRecord.getAttribute("Code"));
						paramData.put("Id",selectedRecord.getAttribute("Id"));
						paramData.put("Continent",selectedRecord.getAttribute("Continent"));
						
						requestData.put("Country",JSOHelper.convertMapToJavascriptObject(paramData));
						
						countryWebService.callOperation(removeOperation, requestData, null, new WebServiceCallback() {
							public void execute(Object[] data, JavaScriptObject xmlDoc,
									RPCResponse rpcResponse, JavaScriptObject wsRequest) {
								
							}
						});
						gridResults.removeSelectedData();
						removeButton.disable();
						updateButton.disable();
					}
				});
                
                layout.addMember(updateButton);

                gridResults.addRecordClickHandler(new RecordClickHandler() {
					public void onRecordClick(RecordClickEvent event) {
						removeButton.enable();
						updateButton.enable();
					}
				});
                
                layout.addMember(gridResults);
                /**
                 * Layout the Grid
                 */
                service.callOperation(getAllOperation, new HashMap<String, String>(), "//Country", new WebServiceCallback() {
					public void execute(Object[] data, JavaScriptObject xmlDoc,
							RPCResponse rpcResponse, JavaScriptObject wsRequest) {
						ListGridRecord[] records = new ListGridRecord[data.length];
						int i = 0;
						for(Object object : data){
							ListGridRecord listGridRecord = new ListGridRecord((JavaScriptObject)object);
							records[i] = listGridRecord;
							i++;
						}
						gridResults.setRecords(records);
					}
				});
                
                final DynamicForm countryForm = new DynamicForm();
                
                TextItem nameItem = new TextItem("Name", "Name");
                TextItem capitalItem = new TextItem("Capital", "Capital");
                TextItem codeItem = new TextItem("Code", "Code");
                TextItem continentItem = new TextItem("Continent", "Continent");
                
                ButtonItem submitItem = new ButtonItem("Submit");
                countryForm.setItems(nameItem,capitalItem,codeItem,continentItem,submitItem);
                
                submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
					public void onClick(
							com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
						countryForm.hide();
						
						Map<String,JavaScriptObject> nestedMap = new LinkedHashMap<String,JavaScriptObject>();
						
						Map<String,String> parameterMap = new LinkedHashMap<String, String>();

						parameterMap.put("Name",countryForm.getValueAsString("Name"));
						parameterMap.put("Capital",countryForm.getValueAsString("Capital"));
						parameterMap.put("Continent",countryForm.getValueAsString("Continent"));
						parameterMap.put("Code",countryForm.getValueAsString("Code"));
						
						nestedMap.put("Country",JSOHelper.convertMapToJavascriptObject(parameterMap));
						
						countryWebService.callOperation(addOperation, nestedMap, "//Country", new WebServiceCallback() {
							
							public void execute(Object[] data, JavaScriptObject xmlDoc,
									RPCResponse rpcResponse, JavaScriptObject wsRequest) {
								ListGridRecord[] prevGridRecords = gridResults.getRecords();
								ListGridRecord[] records = new ListGridRecord[prevGridRecords.length + 1];

								int i = 0;
								for(ListGridRecord listGridRecord : prevGridRecords){
									records[i] = listGridRecord;
									i++;
								}
								records[i] = new ListGridRecord((JavaScriptObject)data[0]);
								gridResults.setRecords(records);
							}
						});
						
					}
				});
                
                addButton.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						countryForm.show();
					}
				});
                countryForm.hide();
                layout.addMember(countryForm);
                canvas.addChild(layout);  

                SC.clearPrompt();  
                canvas.draw();
            }  
        });
        
    }  
    
}
