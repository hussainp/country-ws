package com.synerzip.country.client.data;

import java.util.HashMap;

import com.google.gwt.core.client.JavaScriptObject;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.WebService;
import com.smartgwt.client.data.WebServiceCallback;
import com.smartgwt.client.data.fields.DataSourceBooleanField;
import com.smartgwt.client.data.fields.DataSourceDateField;
import com.smartgwt.client.data.fields.DataSourceFloatField;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.data.fields.DataSourceLinkField;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.rpc.RPCResponse;
import com.smartgwt.client.util.SC;


/**
 * 
 * @author hussainp
 *
 * A WebService based DataSource to facilitate loading 
 * of Country data
 *
 */
public class CountryWSDS extends DataSource {
	private static CountryWSDS instance = null;
	private static final String serviceNamespace = "http://synerzip.com/m/schemas";
	
	private Object[] resultElement;
	
    public static CountryWSDS getInstance() {  
        if (instance == null) {  
            instance = new CountryWSDS("countryDS");  
        }  
        return instance;  
    }
    
    private CountryWSDS(String id){
    	setID(id);  
        setRecordXPath("/Countries/Country");  
        DataSourceIntegerField pkField = new DataSourceIntegerField("pk");  
        pkField.setHidden(true);  
        pkField.setPrimaryKey(true);  
  
        DataSourceTextField countryCodeField = new DataSourceTextField("Code", "Code");  
        countryCodeField.setRequired(true);  
  
        DataSourceTextField countryNameField = new DataSourceTextField("Name", "Country");  
        countryNameField.setRequired(true);  
  
        DataSourceTextField capitalField = new DataSourceTextField("Capital", "Capital");  
  
  
        DataSourceTextField continentField = new DataSourceTextField("continent", "Continent");  
        continentField.setValueMap("Europe", "Asia", "North America", "Australia", "South America", "Africa");  
  
        setFields(pkField, countryCodeField, countryNameField, capitalField, continentField);  
        
        setClientOnly(true); 
    }
    
    private void fetchAll(){
    	WebService webService = WebService.get(serviceNamespace);

    	webService.callOperation("getAll", new HashMap<String,String>(),"//Countries", new WebServiceCallback() {
			public void execute(Object[] data, JavaScriptObject xmlDoc,
					RPCResponse rpcResponse, JavaScriptObject wsRequest) {
				resultElement = data;
			}
		});
    	
    }
}