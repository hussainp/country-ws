package com.synerzip.country.server.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.document.mongodb.mapping.Document;

import com.mongodb.BasicDBObject;

@Document
public class Country extends BasicDBObject{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1104544507910439600L;
	
	@Id
	private String id;
	
	private String name;
	private String capital;
	private String code;
	private String continent;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCapital() {
		return capital;
	}
	public void setCapital(String capital) {
		this.capital = capital;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getContinent() {
		return continent;
	}
	public void setContinent(String continent) {
		this.continent = continent;
	}
}
