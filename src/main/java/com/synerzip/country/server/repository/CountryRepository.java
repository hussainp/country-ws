package com.synerzip.country.server.repository;

import java.util.List;

import com.synerzip.country.server.model.Country;

public interface CountryRepository {
	Country addCountry(Country country);
	void removeCountry(Country country);
	Country updateCountry(Country country);
	List<Country> getAllCountries();
}
