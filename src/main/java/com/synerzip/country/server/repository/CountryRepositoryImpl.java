package com.synerzip.country.server.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.document.mongodb.MongoOperations;
import org.springframework.data.document.mongodb.query.Criteria;
import org.springframework.data.document.mongodb.query.Query;
import org.springframework.data.document.mongodb.query.Update;
import org.springframework.stereotype.Repository;

import com.synerzip.country.server.model.Country;

@Repository
public class CountryRepositoryImpl implements CountryRepository {
	private Class<Country> countryClazz = Country.class;
	private String collection = "country";
	
	@Autowired
	private MongoOperations mongoOperations;
	
	public MongoOperations getMongoOperations() {
		return mongoOperations;
	}

	public void setMongoOperations(MongoOperations mongoOperations) {
		this.mongoOperations = mongoOperations;
	}

	public Country addCountry(Country country) {
		country.setId(null);
		mongoOperations.save(collection,country);
		Query query = new Query(Criteria.where("code").is(country.getCode()));
		return mongoOperations.findOne(collection,query,countryClazz);
	}

	public void removeCountry(Country country) {
		mongoOperations.remove(collection, new Query(Criteria.where("code").is(country.getCode())), countryClazz);
	}

	public Country updateCountry(Country country) {
		mongoOperations.save(collection,country);
		return fetchByCode(country.getCode());
	}

	private Country fetchByCode(String code){
		Query query = new Query(Criteria.where("code").is(code));
		return mongoOperations.findOne(collection,query,countryClazz);
	}
	
	public List<Country> getAllCountries() {
		return (List<Country>)mongoOperations.getCollection(collection,countryClazz);
	}

}
