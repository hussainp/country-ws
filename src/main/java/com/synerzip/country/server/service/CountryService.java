package com.synerzip.country.server.service;

import java.util.List;

import com.synerzip.country.server.model.Country;

public interface CountryService {
	Country addCountry(Country country);
	void removeCountry(Country country);
	Country updateCountry(Country country);
	List<Country> getAllCountries();
}
