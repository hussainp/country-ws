package com.synerzip.country.server.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.synerzip.country.server.model.Country;
import com.synerzip.country.server.repository.CountryRepository;

@Service
public class CountryServiceImpl implements CountryService {

	@Autowired
	private CountryRepository countryRepository;
	
	public CountryRepository getCountryRepository() {
		return countryRepository;
	}

	public void setCountryRepository(CountryRepository countryRepository) {
		this.countryRepository = countryRepository;
	}

	public Country addCountry(Country country) {
		return countryRepository.addCountry(country);
	}

	@Transactional
	public void removeCountry(Country country) {
			countryRepository.removeCountry(country);
	}

	@Transactional
	public Country updateCountry(Country country) {
		return countryRepository.updateCountry(country);
	}

	public List<Country> getAllCountries() {
		return countryRepository.getAllCountries();
	}

}
