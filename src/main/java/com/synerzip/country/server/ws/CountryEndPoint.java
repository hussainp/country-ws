package com.synerzip.country.server.ws;

import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.Namespace;
import org.jdom.xpath.XPath;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import org.springframework.ws.soap.server.endpoint.annotation.SoapAction;

import com.synerzip.country.server.model.Country;
import com.synerzip.country.server.service.CountryService;

@Endpoint
public class CountryEndPoint {
	private static final Logger logger = Logger.getLogger(CountryEndPoint.class);
	
	private static final String NAMESPACE_URI = "http://synerzip.com/m/schemas";
	private static final String NAMESPACE_PREFIX = "message";
	private XPath nameExpression;
	private XPath codeExpression;
	private XPath continentExpression;
	private XPath idExpression;
	private XPath capitalExpression;

	private CountryService countryService;

	@Autowired
	public CountryEndPoint(CountryService countryService) throws JDOMException {
		this.countryService = countryService;

		Namespace namespace = Namespace.getNamespace("m", NAMESPACE_URI);

		nameExpression = XPath.newInstance("//m:Name");
		nameExpression.addNamespace(namespace);

		codeExpression = XPath.newInstance("//m:Code");
		codeExpression.addNamespace(namespace);

		capitalExpression = XPath.newInstance("//m:Capital");
		capitalExpression.addNamespace(namespace);

		idExpression = XPath.newInstance("//m:Id");
		idExpression.addNamespace(namespace);

		continentExpression = XPath.newInstance("//m:Continent");
		continentExpression.addNamespace(namespace);
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "addRequest")
	@SoapAction(value="http://synerzip.com/m/schemas/add")
	@ResponsePayload
	public Element addCountry(@RequestPayload Element countryElement)
		throws JDOMException {
		Element element = new Element("addResponse",NAMESPACE_PREFIX,NAMESPACE_URI);
		element.addContent(processResponse(countryService.addCountry(processRequest(countryElement))));
		return element;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "removeRequest")
	@SoapAction(value="http://synerzip.com/m/schemas/remove")
	public void deleteCountry(@RequestPayload Element countryElement) throws JDOMException {
		countryService.removeCountry(processRequest(countryElement));
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "updateRequest")
	@SoapAction(value="http://synerzip.com/m/schemas/update")
	@ResponsePayload
	public Element updateCountry(@RequestPayload Element countryElement) throws JDOMException{
		Element element = new Element("updateResponse",NAMESPACE_PREFIX,NAMESPACE_URI);
		element.addContent(processResponse(countryService.updateCountry(processRequest(countryElement))));
		return element;
	}
	
	@PayloadRoot(namespace=NAMESPACE_URI,localPart="getAllRequest")
	@SoapAction(value="http://synerzip.com/m/schemas/getAll")
	@ResponsePayload
	public Element getAllCountries(@RequestPayload Element emptyElement){
		Element countriesElement = null;
		
		try {
			countriesElement = new Element("Countries",NAMESPACE_PREFIX,NAMESPACE_URI);
			for (Country country : countryService.getAllCountries()) {
				countriesElement.addContent(processResponse(country));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			logger.debug("Prepared the response successfully");
		}
		Element responseElement = new Element("getAllResponse",NAMESPACE_PREFIX,NAMESPACE_URI);
		responseElement.addContent(countriesElement);
		return responseElement;
	}
	
	private Country processRequest(Element countryElement) throws JDOMException {
		String name = nameExpression.valueOf(countryElement);
		String capital = capitalExpression.valueOf(countryElement);
		String code = codeExpression.valueOf(countryElement);
		String continent = continentExpression.valueOf(countryElement);
		String id = idExpression.valueOf(countryElement);

		Country country = new Country();
		country.setName(name);
		country.setCapital(capital);
		country.setCode(code);
		country.setContinent(continent);
		country.setId(id);

		return country;
	}
	
	private Element processResponse(Country country){
		Element countryElement = new Element("Country");
		countryElement.addContent((new Element("Name")).addContent(country.getName()));
		countryElement.addContent((new Element("Capital")).addContent(country.getCapital()));
		countryElement.addContent((new Element("Id")).addContent(country.getId()));
		countryElement.addContent((new Element("Code")).addContent(country.getCode()));
		countryElement.addContent((new Element("Continent")).addContent(country.getContinent()));
		
		return countryElement;
		
	}
}
